//
// Created by marcin on 4/9/19.
//

#ifndef ZSO1_CUSTOMER_H
#define ZSO1_CUSTOMER_H

void* thread_customer(void* customer);
void* thread_hairdresser(void* a);
void end_work();
void init_desks();

#endif //ZSO1_CUSTOMER_H
