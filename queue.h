#include "structs.h"

#ifndef ZSO1_QUEUE_H
#define ZSO1_QUEUE_H

void queue_move_left();

int queue_is_empty();

int queue_size();

int queue_insert(struct Customer *customer);

struct Customer* queue_pop();

#endif //ZSO1_QUEUE_H
