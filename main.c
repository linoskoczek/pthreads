#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "queue.h"
#include "processes.h"
#include "structs.h"

int rnd(int divisor) {
    return rand() % divisor;
}

int START_CUSTOMERS = 25;

int main() {
    struct Customer customers[START_CUSTOMERS];
    struct Hairdresser hairdressers[4];
    srand(time(NULL));

    init_desks();

    for(int i = 1; i <= 4; i++) {
        hairdressers[i-1].id = i;
        hairdressers[i-1].desk = -1;
        pthread_create(&hairdressers[i-1].thread, NULL, thread_hairdresser, &hairdressers[i-1]);
    }

    usleep(100);

    for(int i = 1; i <= START_CUSTOMERS; i++) {
        customers[i-1].id = i;
        customers[i-1].desk = -1;
        customers[i-1].paid = 0;
        customers[i-1].finished = 0;
        pthread_create(&customers[i-1].thread, NULL, thread_customer, &customers[i-1]);
        if(SLEEP_ON) usleep(rnd(500000));
    }

    for(int i = 1; i <= START_CUSTOMERS; i++) {
        pthread_join(customers[i-1].thread, NULL);
    }
    usleep(rnd(500000));
    end_work();
    for(int i = 1; i <= 4; i++) {
        pthread_join(hairdressers[i-1].thread, NULL);
    }
    printf("end\n");

    return EXIT_SUCCESS;
}