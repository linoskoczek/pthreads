cmake_minimum_required(VERSION 3.12)
project(ZSO1 C)


set(CMAKE_C_STANDARD 11)

add_executable(ZSO1 main.c queue.c queue.h cash_queue.c cash_queue.h processes.h processes.c structs.h)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -pthread")
target_compile_options(ZSO1 PRIVATE -pthread)