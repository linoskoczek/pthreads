#include "queue.h"
#include "cash_queue.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define MAX_C 16

pthread_mutex_t mutex_cashier = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_desk_assigner = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_manager = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_client_assignment = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_sleep = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_can_pay = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_waiting_for_desk = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_dressing_finished = PTHREAD_COND_INITIALIZER;

int desk[5];
int work = 1;

void init_desks() {
    for (int i = 0; i < 5; i++) {
        desk[i] = 0;
    }
}

int assign_to_desk(int hairdresser_id) {
    int found = -1;
    for (int i = 0; i < 5; i++) {
        if (desk[i] == 0) {
            desk[i] = hairdresser_id;
            found = i;
            break;
        }
    }
    pthread_cond_broadcast(&cond_client_assignment);
    return found;
}

void end_work() {
    printf("Notifying about the end of work.\n");
    pthread_mutex_lock(&mutex_manager);
    work = 0;
    pthread_mutex_unlock(&mutex_manager);
    pthread_mutex_lock(&mutex_cashier);
    pthread_cond_broadcast(&cond_sleep);
    pthread_mutex_unlock(&mutex_cashier);
}

void free_desk(int desk_id) {
    desk[desk_id] = 0;
    pthread_cond_broadcast(&cond_waiting_for_desk);
}

void *thread_hairdresser(void *h) {
    struct Hairdresser *hairdresser = (struct Hairdresser *) h;
    int id = hairdresser->id;

    int work_local = 1;
    while (work_local) {

        int error = pthread_mutex_trylock(&mutex_cashier);
        if (error == 0) {
            pthread_cond_wait(&cond_sleep, &mutex_cashier);
            struct Customer *client = cash_queue_pop();
            if (client != NULL) {
                printf("Hairdresser %d takes money from customer %d\n", id, client->id);
                if (SLEEP_ON) usleep(rnd(2000000));
                client->paid = 1;
                pthread_cond_broadcast(&cond_can_pay);
            }
            pthread_mutex_unlock(&mutex_cashier);
        }

        error = pthread_mutex_trylock(&mutex_desk_assigner);
        if (error == 0) {
            struct Customer *client = queue_pop();
            if (client != NULL) {
                int assigned_desk = assign_to_desk(id);
                while (assigned_desk == -1) {
                    printf("No free desk for hairdresser %d\n", id);
                    pthread_cond_wait(&cond_waiting_for_desk, &mutex_desk_assigner);
                    assigned_desk = assign_to_desk(id);
                }
                hairdresser->desk = assigned_desk;
                client->desk = hairdresser->desk;
                printf("Customer %d will be cut by hairdresser %d on desk %d\n", client->id, id, client->desk);
                if (SLEEP_ON) usleep(rnd(2000000));
                client->finished = 1;
                pthread_cond_broadcast(&cond_dressing_finished);
            }
            pthread_mutex_unlock(&mutex_desk_assigner);
        }

        pthread_mutex_lock(&mutex_manager);
        work_local = work;
        pthread_mutex_unlock(&mutex_manager);
    }

    printf("Hairdresser %d ends the work.\n", id);
    pthread_exit(EXIT_SUCCESS);
}

void *thread_customer(void *c) {
    struct Customer *customer = (struct Customer *) c;
    int id = customer->id;

    // waiting
    printf("Customer %d enters the hairdresser's\n", id);
    if (queue_size() + cash_queue_size() >= MAX_C || queue_insert(customer) != 0) {
        printf("No place for more customers.\n");
        pthread_exit(EXIT_SUCCESS);
    }

    // cutting
    pthread_mutex_lock(&mutex_desk_assigner);

    while (customer->desk < 0) {
        pthread_mutex_lock(&mutex_cashier);
        pthread_cond_broadcast(&cond_sleep);
        pthread_mutex_unlock(&mutex_cashier);
        pthread_cond_wait(&cond_client_assignment, &mutex_desk_assigner);
    }

    printf("Customer %d is being cut on desk %d\n", id, customer->desk);
    while (customer->finished == 0) {
        pthread_cond_wait(&cond_dressing_finished, &mutex_desk_assigner);
    }
    free_desk(customer->desk);
    printf("Customer %d leaves desk %d\n", id, customer->desk);

    pthread_mutex_unlock(&mutex_desk_assigner);

    // paying
    pthread_mutex_lock(&mutex_cashier);
    printf("Customer %d is willing to pay\n", id);
    cash_queue_insert(customer);
    while (customer->paid == 0) {
        pthread_cond_broadcast(&cond_sleep);
        pthread_cond_wait(&cond_can_pay, &mutex_cashier);
    }
    pthread_mutex_unlock(&mutex_cashier);
    printf("Customer %d finished\n", id);
    pthread_exit(EXIT_SUCCESS);
}
