#include "structs.h"

#ifndef ZSO1_CASH_QUEUE_H
#define ZSO1_CASH_QUEUE_H

void cash_queue_left();

int cash_queue_empty();

int cash_queue_size();

int cash_queue_insert(struct Customer* customer);

struct Customer* cash_queue_pop();

#endif //ZSO1_CASH_QUEUE_H
