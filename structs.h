#include <pthread.h>

#ifndef ZSO1_STRUCTS_H
#define ZSO1_STRUCTS_H
#define SLEEP_ON 1

int rnd();

struct Customer {
    int id;
    pthread_t thread;
    int desk;
    int finished;
    int paid;
};

struct Hairdresser {
    int id;
    pthread_t thread;
    int desk;
};

#endif //ZSO1_STRUCTS_H
