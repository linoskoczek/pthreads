#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "queue.h"

#define MAX 16
#define SOFA 5

pthread_mutex_t mutex_queue = PTHREAD_MUTEX_INITIALIZER;

struct Customer* waiting[MAX];
int queue_front = 0;
int queue_count = 0;

void queue_move_left() {
    for (int i = 0; i < queue_front - 1; i++) {
        if (i == SOFA) {
            printf("Customer moves from waiting room to a sofa\n");
        }
        waiting[i] = waiting[i + 1];
    }
    queue_front--;
    waiting[queue_front] = 0;
}

int queue_is_empty() {
    return queue_count == 0;
}

int queue_is_full() {
    return queue_count == MAX;
}

int queue_size() {
    pthread_mutex_lock(&mutex_queue);
    int result = queue_count;
    pthread_mutex_unlock(&mutex_queue);
    return result;
}

int queue_insert(struct Customer *customer) {
    int ret = -1;
    pthread_mutex_lock(&mutex_queue);
    if (!queue_is_full()) {
        waiting[queue_front++] = customer;
        queue_count++;
        ret = 0;
    } else {
        printf("No space in a waiting room. Customer goes away.\n");
    }
    pthread_mutex_unlock(&mutex_queue);
    return ret;
}

void queue_display() {
    for(int i = 0; i < MAX; i++) {
        printf("%d ", waiting[i]->id);
    }
    printf("\n");
}

struct Customer* queue_pop() {
    pthread_mutex_lock(&mutex_queue);
    //queue_display();
    if (!queue_is_empty()) {
        struct Customer* data = waiting[0];
        printf("Customer %d leaves a sofa\n", (*data).id);

        queue_move_left();

        queue_count--;
        pthread_mutex_unlock(&mutex_queue);
        return data;
    } else {
        pthread_mutex_unlock(&mutex_queue);
        return NULL;
    }
}
