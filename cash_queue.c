#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "queue.h"

#define MAX 20

pthread_mutex_t mutex_cash_queue = PTHREAD_MUTEX_INITIALIZER;

struct Customer* cash_queue[MAX];
int cash_queue_front = 0;
int cash_queue_count = 0;

void cash_queue_left() {
    for (int i = 0; i < cash_queue_front - 1; i++) {
        cash_queue[i] = cash_queue[i + 1];
    }
    cash_queue_front--;
    cash_queue[cash_queue_front] = 0;
}

int cash_queue_empty() {
    return cash_queue_count == 0;
}

int cash_queue_full() {
    return cash_queue_count == MAX;
}

int cash_queue_size() {
    pthread_mutex_lock(&mutex_cash_queue);
    int result = cash_queue_count;
    pthread_mutex_unlock(&mutex_cash_queue);
    return result;
}

int cash_queue_insert(struct Customer* customer) {
    int ret = -1;
    pthread_mutex_lock(&mutex_cash_queue);
    //printf("Customer %d enters the queue\n", id);
    if (!cash_queue_full()) {
        //cash_queue[cash_queue_front] = (struct Customer*) malloc( sizeof(struct Customer) );
        cash_queue[cash_queue_front++] = customer;
        cash_queue_count++;
        ret = 0;
    } else {
        printf("No space by cash.\n");
    }
    pthread_mutex_unlock(&mutex_cash_queue);
    return ret;
}

void cash_queue_display() {
    for(int i = 0; i < MAX; i++) {
        printf("%d ", cash_queue[i]->id);
    }
    printf("\n");
}

struct Customer* cash_queue_pop() {
    pthread_mutex_lock(&mutex_cash_queue);
    //cash_queue_display();
    if (!cash_queue_empty()) {
        struct Customer* data = cash_queue[0];
        printf("Customer %d is going to pay\n", (*data).id);

        cash_queue_left();

        cash_queue_count--;
        pthread_mutex_unlock(&mutex_cash_queue);
        return data;
    } else {
        pthread_mutex_unlock(&mutex_cash_queue);
        return NULL;
    }
}
